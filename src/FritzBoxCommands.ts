export enum FritzBoxCommands {
  getSwitchList = "getswitchlist",
  getSwitchPower = "getswitchpower",
  getTemperature = "gettemperature",
  getSwitchName = "getswitchname",
}
