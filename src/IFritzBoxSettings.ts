export interface IFritzBoxSettings {
  username: string
  password: string
  ip: string
  useSsl: boolean
}
