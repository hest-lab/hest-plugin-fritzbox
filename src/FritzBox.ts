import { IFritzBoxSettings } from './IFritzBoxSettings'
import axios from 'axios'
import * as crypto from 'crypto'
import { FritzBoxCommands } from './FritzBoxCommands'

export class FritzBox {
  private readonly defaultSessionId = '0000000000000000'
  private sessionId: string

  constructor(protected readonly settings: IFritzBoxSettings) {}

  public async login(): Promise<void> {
    const challengeRes = await axios.get<string>(
      `http://${this.settings.ip}/login_sid.lua`,
    )
    const challenge = challengeRes.data.match('<Challenge>(.*?)</Challenge>')[1]
    const digest = crypto
      .createHash('md5')
      .update(Buffer.from(challenge + '-' + this.settings.password, 'utf16le'))
      .digest('hex')
    const response = challenge + '-' + digest
    const url = `http://${this.settings.ip}/login_sid.lua?username=${this.settings.username}&response=${response}`
    const nextRes = await axios.get<string>(url)
    const sessionId = nextRes.data.match('<SID>(.*?)</SID>')[1]
    if (sessionId === this.defaultSessionId) {
      throw new Error('Invalid')
    }
    this.sessionId = sessionId
  }

  private async request<T>(cmd: FritzBoxCommands, ain?: string): Promise<T> {
    let url = `http://${this.settings.ip}/webservices/homeautoswitch.lua?&sid=${this.sessionId}&switchcmd=${cmd}`
    if (ain) {
      url += `&ain=${ain}`
    }
    const res = await axios.get<T>(url)
    return res.data
  }

  public async getSwitchList(): Promise<Array<string>> {
    const res = await this.request<string>(FritzBoxCommands.getSwitchList)
    return res.trim().split(',')
  }

  /**
   * @param ain
   * @returns Milliwatt
   */
  public async getSwitchPower(ain: string): Promise<number> {
    const res = await this.request<string>(FritzBoxCommands.getSwitchPower, ain)
    return +res
  }

  /**
   * @param ain
   * @returns Celcius
   */
  public async getTemperature(ain: string): Promise<number> {
    const res = await this.request<string>(FritzBoxCommands.getTemperature, ain)
    return +res / 10
  }

  public async getSwitchName(ain: string): Promise<string> {
    return await this.request<string>(FritzBoxCommands.getSwitchName, ain)
  }
}
