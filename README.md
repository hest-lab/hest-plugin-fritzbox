# Fritzbox plugin

[![pipeline status](https://gitlab.com/hest-lab/hest-plugin-fritzbox/badges/master/pipeline.svg)](https://gitlab.com/hest-lab/hest-plugin-fritzbox/-/commits/master)
[![coverage report](https://gitlab.com/hest-lab/hest-plugin-fritzbox/badges/master/coverage.svg)](https://gitlab.com/hest-lab/hest-plugin-fritzbox/-/commits/master)

## Example

```ts
// Create instance
const fb = new FritzBox({
  ip: "192.168.0.1",
  username: "xxxxx",
  password: "xxxxx",
  useSsl: false,
})
// Login
await fb.login()
// Get switch list
const ains = await fb.getSwitchList()
// Get temperature
fb.getTemperature(ains[0])
// Get switch name
fb.getSwitchName(ains[0])
```
