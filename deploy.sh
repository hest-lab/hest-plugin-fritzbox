#!/usr/bin/env bash

set -x
set -e
echo "--------- build project ----------"
git pull
npm run lint
npm run build
#npm run test
npm version patch
npm publish
git push --follow-tags
